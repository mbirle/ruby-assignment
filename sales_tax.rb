class Product
  @@sales_tax = 0
  @@total_bill = 0
  attr_accessor :name, :quantity
  def initialize(name, quantity, cost, is_freeT=false, is_imported=false)
    @name = name
    @quantity = quantity
    @raw_cost = cost
    @is_freeT= is_freeT
    @is_imported = is_imported
    @tax = calc_final(@raw_cost)
    Product.update_sales_tax(@tax)
    Product.update_total_bill(@raw_cost + @tax)
  end

  # Calculate Sales TAX i.e 10%
  def calc_tax(cost)
    cost*10/100
  end

  #Calculate Imported Sales TAX i.e 5%
  def calc_import_tax(cost)
    cost*5/100
  end

  #Calculate Final TAX on product
  def calc_final(cost)
    total = 0
    total += calc_tax(cost) if !@is_freeT
    total += calc_import_tax(cost) if @is_imported
    return total
  end

  def get_total
    (@raw_cost + @tax).round(2)
  end

  #class methodes
  def self.update_sales_tax(value)
    @@sales_tax += value
  end

  def self.update_total_bill(value)
    @@total_bill += value
  end                

  def self.get_sales_tax
    @@sales_tax.round(2)
  end

  def self.get_total_bill
    @@total_bill.round(2)
  end
end


def main
  all_item = []
  free_tax = ['pills','pill','chocolate','chocolates','food','book','books']

  puts "Enter 0 to stop."
  while true
    item_string = gets.chomp
    break if item_string == "0"

    item_arr = item_string.split(" ")
    quantity = item_arr[0].to_i

    #check product is imported or not
    is_imported = item_arr.include? 'imported'

    #check product is free from tax or not
    is_freeT = !(item_arr & free_tax).empty?
    # is_freeT =  item_arr.any?{ |item| free_tax.include?(item) }
    # is_freeT = item_arr.find{ |i| free_tax.include? i} != nil

    item_name = item_arr[1..-3].join(" ") # Product Name
    cost = item_arr.last.to_f              # Product Cost
    item = Product.new(item_name,quantity,cost,is_freeT,is_imported) # Create Product
    all_item.push(item)                 # Pushed it on the list
  end

  #Prints Output
  all_item.each {|i| puts "#{i.quantity} #{i.name} : #{i.get_total}"}
  puts "Sales Taxes : #{Product.get_sales_tax}"
  puts "Total : #{Product.get_total_bill}"
end
main()