module Validate
  def check_input?(input_array)
    return false if (!is_integer?(input_array[0]) ||  input_array[0].to_i <= 0)
    return false if (!is_float?(input_array[-1]) ||  input_array[-1].to_f <= 0)
    return false if (input_array[-2] != "at")
    return true
  end

  def is_integer?(input_string)
    !!Integer(input_string) rescue false
  end

  def is_float?(input_string)
    !!Float(input_string) rescue false
  end
end