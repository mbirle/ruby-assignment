require_relative 'batch'
require_relative 'product'
require_relative 'billing'

$free_tax = ['pills','pill','chocolate','chocolates','food','book','books']

def start
  print "Enter Number of Batches : "
  number_of_batches = gets.chomp.to_i

  bill = Billing.new(number_of_batches)
  while(number_of_batches > 0)
    number_of_batches -= 1
    print "Enter Number of Item : "
    items = gets.chomp.to_i
    batch = Batch.new(items)

    while(items > 0)
      items -= 1
      details = gets.chomp
      details = details.split(" ")
      quantity = details[0].to_i      # Item Quantity
      item_name = details[1..-3].join(" ") # Item Name
      is_imported = details.include? 'imported' # Is it Imported
      is_freeT = !(details & $free_tax).empty?  # Is is Free Tax
      cost = details.last.to_f        # Cost
      product = Product.new(item_name, quantity, cost, is_freeT, is_imported)
      batch.add_item(product)
    end 
    bill.add_batch(batch)
  end
  puts bill
end

start