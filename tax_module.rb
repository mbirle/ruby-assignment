module TaxModule
  def add_item(product)
    begin
      update_tax(product.sales_tax)
      update_cost(product.total_cost)
      @items_list.push(product)
      return true
    rescue
      return false
    end
  end

  def update_tax(tax)
    @sales_tax += tax
  end

  def update_cost(cost)
    @total_cost += cost
  end

  def items_list_to_string(items_list)
    output_string = ""
    items_list.each do |item|
      output_string += item.to_s
    end 
    return output_string
  end
end
