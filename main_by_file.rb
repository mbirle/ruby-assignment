require_relative 'batch'
require_relative 'product'
require_relative 'billing'
require_relative 'validation'

class GetInput
  def initialize(input_list)
    @index = -1
    @input_list = input_list
  end
  def next_input()
    @index += 1
    @input_list[@index]
  end
end

class Main
  include Validate
  def main
    free_tax = ['pills','pill','chocolate','chocolates','food','book','books']
    input_list = File.read("input.txt").split("\n")
    input = GetInput.new input_list
    number_of_batches = input.next_input.to_i

    bill = Billing.new
    for i in 0...number_of_batches do
      number_of_items = input.next_input.to_i
      batch = Batch.new

      for j in 0...number_of_items do
        input_string = input.next_input
        details = input_string.split(" ")
        if ( check_input?(details) )
          quantity = details[0].to_i      # Item Quantity
          item_name = details[1..-3].join(" ") # Item Name
          is_imported = details.include? 'imported' # Is it Imported
          is_free_tax = !(details & free_tax).empty?  # Is is Free Tax
          cost = details.last.to_f        # Cost
          product = Product.new(item_name, quantity, cost, is_free_tax, is_imported)
          status = batch.add_item(product)
          if status
            puts item_name+" added successfully"
          else
            puts "Error in adding item"
          end
        else
          puts "Invalid Input : "+input_string
        end
      end 
      bill.add_item(batch)
    end
    puts bill
    File.write('output.txt',bill,mode:'w')
  end
end
run = Main.new
run.main()