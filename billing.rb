require_relative "tax_module"

class Billing
  include TaxModule
  attr_reader :sales_tax ,:total_cost, :items_list

  def initialize
    @sales_tax = 0
    @total_cost = 0
    @items_list = Array.new()
  end
  def to_s()
    output_string = items_list_to_string(@items_list)
    output_string += "\nGrand Sales Tax : #{@sales_tax.round(2)}\n"
    output_string += "Grand Total Cost : #{@total_cost.round(2)}\n"
    return output_string
  end
end
