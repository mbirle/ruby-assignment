class Product
  attr_accessor :tax, :cost, :sales_tax, :total_cost
  def initialize(name, quantity, raw_cost, is_free_tax=false, is_imported=false)
    @name = name
    @quantity = quantity
    @raw_cost = raw_cost
    @is_free_tax= is_free_tax
    @is_imported = is_imported
    @sales_tax = calc_final(@raw_cost)
    @total_cost = @raw_cost + @sales_tax
    @status = true
  end

  # Calculate Sales TAX i.e 10%
  def calc_tax(cost)
    cost*10/100
  end

  #Calculate Imported Sales TAX i.e 5%
  def calc_import_tax(cost)
    cost*5/100
  end

  #Calculate Final TAX on product
  def calc_final(cost)
    total = 0
    total += calc_tax(cost) if !@is_free_tax
    total += calc_import_tax(cost) if @is_imported
    return total
  end

  def to_s
    return "#{@quantity} #{@name} : #{@total_cost.round(2)}\n"
  end
end